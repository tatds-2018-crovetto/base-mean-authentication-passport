/* eslint-disable max-len */
// eslint-disable-next-line no-unused-vars
const mocha = require('mocha');
const chai = require('chai');
const Comment = require('../models/comment');
const User = require('../models/user');
const Reply = require('../models/reply');
const chaiHttp = require('chai-http');
// eslint-disable-next-line no-unused-vars
const should = chai.should();
chai.use(chaiHttp);

const app = require('../app');

describe('Comments', () => {
  beforeEach((done) => { // Before each test we empty the users collection
    Reply.deleteMany({}, (err) => {});
    Comment.deleteMany({}, (err) => {});
    User.deleteMany({}, (err) => {});
    done();
  });
  describe('Comments routes', () => {
    describe('POST /comments/add', () => {
      it('Should create user and authenticate it then POST a comment and register it', (done) => {
        const user = new User({
          name: 'Comment Tester',
          email: 'comment@test.com',
          username: 'comments_tester',
          password: 'qwerty',
        });
        // Add user to db and check
        User.addUser(user, ( ) => {
          User.getUserById(user._id, (err, fetchedUser) => {
            const req = {
              email: fetchedUser.email,
              password: 'qwerty',
            };
            chai.request(app)
                .post('/users/authenticate')
                .send(req)
                .then((res)=>{
                  const token = res.body.token;
                  const req = {
                    author: fetchedUser._id,
                    content: 'TEST COMMENT',
                    movie_id: '550',
                  };
                  chai.request(app)
                      .post('/comments/add')
                      .set('Authorization', token)
                      .send(req)
                      .then((res)=>{
                        res.body.should.have.property('success').equal(true);
                        res.body.should.have.property('message').equal('Comment stored.');
                        res.body.should.have.property('comment').to.be.a('object');
                        res.body.comment.should.have.property('_id').to.be.a('string');
                        res.body.comment.should.have.property('author').equal(fetchedUser._id.toString());
                        res.body.comment.should.have.property('likes').to.be.a('array').that.is.empty;
                        res.body.comment.should.have.property('replies').to.be.a('array').that.is.empty;
                        res.body.comment.should.have.property('content').equal('TEST COMMENT');
                        res.body.comment.should.have.property('createdAt');
                        res.body.comment.should.have.property('updatedAt');
                        done();
                      });
                });
          });
        });
      });
      it('Should return 401 Unauthorized if Authorization header is missing', (done) => {
        const req = {
          author: 'Some author x',
          content: 'TEST COMMENT',
          movie_id: '550',
        };
          // Request without Authorization header
        chai.request(app)
            .post('/comments/add')
            .send(req)
            .end((err, res)=>{
              res.should.have.status(401);
              done();
            });
      });
      it('Should send 401 Unauthorized because token corresponding user not match author', (done) => {
        const user = new User({
          name: 'Comment Tester',
          email: 'comment@test.com',
          username: 'comments_tester',
          password: 'qwerty',
        });
          // Add user to db and check
        User.addUser(user, ( ) => {
          User.getUserById(user._id, (err, fetchedUser) => {
            const req = {
              email: fetchedUser.email,
              password: 'qwerty',
            };
            chai.request(app)
                .post('/users/authenticate')
                .send(req)
                .then((res)=>{
                  const token = res.body.token;
                  const req = {
                    author: 'invalid author id',
                    content: 'TEST COMMENT',
                    movie_id: '550',
                  };
                  chai.request(app)
                      .post('/comments/add')
                      .set('Authorization', token)
                      .send(req)
                      .then((res)=>{
                        res.should.have.status(401);
                        done();
                      });
                });
          });
        });
      });
      it('Should send Unauthorized 401 because missing author parameter', (done) => {
        const user = new User({
          name: 'Comment Tester',
          email: 'comment@test.com',
          username: 'comments_tester',
          password: 'qwerty',
        });
        // Add user to db and check
        User.addUser(user, ( ) => {
          User.getUserById(user._id, (err, fetchedUser) => {
            const req = {
              email: fetchedUser.email,
              password: 'qwerty',
            };
            chai.request(app)
                .post('/users/authenticate')
                .send(req)
                .then((res)=>{
                  const token = res.body.token;
                  const req = {
                    content: 'TEST COMMENT',
                    movie_id: '550',
                  };
                  chai.request(app)
                      .post('/comments/add')
                      .set('Authorization', token)
                      .send(req)
                      .then((res)=>{
                        res.should.have.status(401);
                        done();
                      });
                });
          });
        });
      });
      it('Should send bad request 400 because missing content parameter', (done) => {
        const user = new User({
          name: 'Comment Tester',
          email: 'comment@test.com',
          username: 'comments_tester',
          password: 'qwerty',
        });
        // Add user to db and check
        User.addUser(user, ( ) => {
          User.getUserById(user._id, (err, fetchedUser) => {
            const req = {
              email: fetchedUser.email,
              password: 'qwerty',
            };
            chai.request(app)
                .post('/users/authenticate')
                .send(req)
                .then((res)=>{
                  const token = res.body.token;
                  const req = {
                    author: fetchedUser._id,
                    movie_id: '550',
                  };
                  chai.request(app)
                      .post('/comments/add')
                      .set('Authorization', token)
                      .send(req)
                      .then((res)=>{
                        res.should.have.status(400);
                        done();
                      });
                });
          });
        });
      });
    });

    describe('GET /comments/', ()=>{
      it('Should return empty array due empty db', (done) => {
        chai.request(app)
            .get('/comments/')
            .end((err, res)=>{
              chai.expect(err).to.be.null;
              res.should.have.status(200);
              res.body.should.have.property('success').equal(true);
              res.body.should.have.property('message');
              res.body.should.have.property('comments').to.be.a('array').that.is.empty;
              done();
            });
      });
      it('Should return comments array with 6 comments, one with reply object populated', (done) => {
        const user = new User({
          name: 'Comment Tester',
          email: 'comment@test.com',
          username: 'comments_tester',
          password: 'qwerty',
        });
        const comments = [
          new Comment({
            author: user._id,
            content: 'test comment',
            movie_id: '550',
          }),
          new Comment({
            author: user._id,
            content: 'test comment',
            movie_id: '550',
          }),
          new Comment({
            author: user._id,
            content: 'test comment',
            movie_id: '550',
          }),
          new Comment({
            author: user._id,
            content: 'test comment',
            movie_id: '550',
          }),
          new Comment({
            author: user._id,
            content: 'test comment',
            movie_id: '550',
          }),
        ];
        User.addUser(user, (err, newUser)=>{
          const commentWithReply = new Comment({
            author: user._id,
            content: 'COMMENT WITH REPLY',
            movie_id: '550',
          });
          Comment.addComment(commentWithReply, () => {
            const reply = new Reply({
              author: user._id,
              content: 'REPLY TEST',
              replies_to: commentWithReply._id,
            });
            Reply.addReply(reply, ()=>{});
            // I don't know why but if modify or remove the next line reply doesn't register
            Comment.getCommentById(commentWithReply._id.toString(), (err, c)=>{});
          });
          // Store 5 comments
          comments.forEach((comment)=>{
            Comment.addComment(comment, ()=>{});
          });


          chai.request(app)
              .get('/comments/')
              .end((err, res)=>{
                res.should.have.status(200);
                res.body.should.have.property('success').equal(true);
                res.body.should.have.property('message').equal('Comments fetched.');
                res.body.should.have.property('comments').to.be.a('array').that.have.lengthOf(6);
                const pos = res.body.comments.findIndex((comment) => comment.replies.length > 0 );
                const commentWithReply = res.body.comments.find((comment) => comment.replies.length > 0);
                commentWithReply.should.have.property('author').to.be.a('object');
                commentWithReply.author.should.have.property('_id').equal(newUser._id.toString());
                commentWithReply.author.should.not.have.property('password');
                commentWithReply.should.have.property('likes').to.be.a('array').that.is.empty;
                commentWithReply.should.have.property('replies').to.be.a('array').that.have.lengthOf(1);
                commentWithReply.replies[0].should.have.property('author').to.be.a('object');
                commentWithReply.replies[0].should.have.property('replies_to').to.be.a('string').equal(commentWithReply._id);
                commentWithReply.replies[0].should.have.property('content').equal('REPLY TEST');
                commentWithReply.should.have.property('content').to.be.a('string').equal(`COMMENT WITH REPLY`);
                commentWithReply.should.have.property('createdAt').to.be.a('string');
                commentWithReply.should.have.property('updatedAt').to.be.a('string');
                res.body.comments.splice(pos, 1);
                res.body.comments.forEach((comment)=>{
                  comment.should.have.property('author').to.be.a('object');
                  comment.author.should.have.property('_id').equal(newUser._id.toString());
                  comment.author.should.not.have.property('password');
                  comment.should.have.property('likes').to.be.a('array').that.is.empty;
                  comment.should.have.property('replies').to.be.a('array').that.is.empty;
                  comment.should.have.property('content').to.be.a('string').equal('test comment');
                  comment.should.have.property('createdAt').to.be.a('string');
                  comment.should.have.property('updatedAt').to.be.a('string');
                });
                done();
              });
        });
      });
    });

    describe('GET /comments/:id', ()=>{
      it('Should return NotFound 404 due db is empty', (done) => {
        const user = new User({
          name: 'Comment Tester',
          email: 'comment@test.com',
          username: 'comments_tester',
          password: 'qwerty',
        });
        chai.request(app)
            .get(`/comments/${user._id.toString()}?replies=true`)
            .end((err, res)=>{
              chai.expect(err).to.be.null;
              res.should.have.status(404);
              done();
            });
      });
      it('should return specific comment with replies populated', (done) => {
        const user = new User({
          name: 'Comment Tester',
          email: 'comment@test.com',
          username: 'comments_tester',
          password: 'qwerty',
        });
        const comment = new Comment({
          author: user._id,
          content: `test comment ${user._id}`,
          movie_id: '550',
        });
        const reply = new Reply({
          author: user._id,
          content: 'REPLY TEST',
          replies_to: comment._id,
        });
        User.addUser(user, ()=>{
          Comment.addComment(comment, (err, comment)=>{
            Reply.addReply(reply, ()=>{
              chai.request(app)
                  .get(`/comments/${comment._id}?replies=true`)
                  .end((err, res)=>{
                    chai.expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.comment.should.have.property('replies').to.be.a('array').that.is.not.empty;
                    res.body.comment.replies[0].should.be.a('object');
                    res.body.should.have.property('success').equal(true);
                    res.body.should.have.property('message').equal('Comment w/replies fetched.');
                    res.body.should.have.property('comment').to.be.a('object');
                    res.body.comment.should.have.property('author').to.be.a('object');
                    res.body.comment.author.should.have.property('_id').equal(user._id.toString());
                    res.body.comment.author.should.not.have.property('password');
                    res.body.comment.should.have.property('likes').to.be.a('array').that.is.empty;
                    res.body.comment.replies[0].should.have.property('author').to.be.a('object');
                    res.body.comment.replies[0].should.have.property('_id');
                    res.body.comment.replies[0].should.have.property('content').equal('REPLY TEST');
                    res.body.comment.should.have.property('content').to.be.a('string').equal(comment.content);
                    res.body.comment.should.have.property('createdAt').to.be.a('string');
                    res.body.comment.should.have.property('updatedAt').to.be.a('string');
                    done();
                  });
            });
          });
        });
      });
      it('should return specific comment with replies NOT populated', (done) => {
        const user = new User({
          name: 'Comment Tester',
          email: 'comment@test.com',
          username: 'comments_tester',
          password: 'qwerty',
        });
        const comment = new Comment({
          author: user._id,
          content: `test comment ${user._id}`,
          movie_id: '550',
        });
        const reply = new Reply({
          author: user._id,
          content: 'REPLY TEST',
          replies_to: comment._id,
        });
        User.addUser(user, ()=>{
          Comment.addComment(comment, (err, comment)=>{
            Reply.addReply(reply, ()=>{
              chai.request(app)
                  .get(`/comments/${comment._id}?replies=false`)
                  .end((err, res)=>{
                    chai.expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.comment.should.have.property('replies').to.be.a('array').that.is.not.empty;
                    res.body.comment.replies[0].should.be.a('string').equal(reply._id.toString());
                    res.body.should.have.property('success').equal(true);
                    res.body.should.have.property('message').equal('Comment fetched.');
                    res.body.should.have.property('comment').to.be.a('object');
                    res.body.comment.should.have.property('author').to.be.a('object');
                    res.body.comment.author.should.have.property('_id').equal(user._id.toString());
                    res.body.comment.author.should.not.have.property('password');
                    res.body.comment.should.have.property('likes').to.be.a('array').that.is.empty;
                    res.body.comment.should.have.property('content').to.be.a('string').equal(comment.content);
                    res.body.comment.should.have.property('createdAt').to.be.a('string');
                    res.body.comment.should.have.property('updatedAt').to.be.a('string');
                    done();
                  });
            });
          });
        });
      });
      // Next two tests have bugs

      // it('should return specific comment, the 3rd one with reply', (done) =>{
      //   const user = new User({
      //     name: 'Comment Tester',
      //     email: 'comment@test.com',
      //     username: 'comments_tester',
      //     password: 'qwerty',
      //   });
      //   const comments = [
      //     new Comment({
      //       author: user._id,
      //       content: 'test comment',
      //     }),
      //     new Comment({
      //       author: user._id,
      //       content: 'test comment',
      //     }),
      //     new Comment({
      //       author: user._id,
      //       content: 'test comment',
      //     }),
      //     new Comment({
      //       author: user._id,
      //       content: 'test comment',
      //     }),
      //     new Comment({
      //       author: user._id,
      //       content: 'test comment',
      //     }),
      //   ];
      //   User.addUser(user, (err, newUser)=>{
      //     const commentWithReply = new Comment({
      //       author: user._id,
      //       content: 'COMMENT WITH REPLY',
      //     });
      //     Comment.addComment(commentWithReply, () => {
      //       const reply = new Reply({
      //         author: user._id,
      //         content: 'REPLY TEST',
      //         replies_to: commentWithReply._id,
      //       });
      //       Reply.addReply(reply, ()=>{});
      //       // I don't know why but if modify or remove the next line reply doesn't register
      //       Comment.getCommentById(commentWithReply._id.toString(), (err, c)=>{});
      //     });
      //     // Store 5 comments
      //     comments.forEach((comment)=>{
      //       Comment.addComment(comment, ()=>{});
      //     });
      //     chai.request(app)
      //         .get(`/comments/${commentWithReply._id}?replies=true`)
      //         .end((err, res)=>{
      //           chai.expect(err).to.be.null;
      //           res.should.have.status(200);
      //           res.body.should.have.property('success').equal(true);
      //           res.body.should.have.property('message').equal('Comment w/replies fetched.');
      //           res.body.should.have.property('comment').to.be.a('object');
      //           res.body.comment.should.have.property('author').to.be.a('object');
      //           res.body.comment.author.should.have.property('_id').equal(newUser._id.toString());
      //           res.body.comment.author.should.not.have.property('password');
      //           res.body.comment.should.have.property('likes').to.be.a('array').that.is.empty;
      //           res.body.comment.should.have.property('replies').to.be.a('array').that.is.not.empty;
      //           res.body.comment.replies[0].should.have.property('author').to.be.a('object');
      //           res.body.comment.replies[0].should.have.property('_id');
      //           res.body.comment.replies[0].should.have.property('content').equal('REPLY TEST');
      //           res.body.comment.should.have.property('content').to.be.a('string').equal(commentWithReply.content);
      //           res.body.comment.should.have.property('createdAt').to.be.a('string');
      //           res.body.comment.should.have.property('updatedAt').to.be.a('string');
      //           done();
      //         });
      //   });
      // });
      // it('should return specific comment, the 3rd one with  NO reply', (done) =>{
      //   const user = new User({
      //     name: 'Comment Tester',
      //     email: 'comment@test.com',
      //     username: 'comments_tester',
      //     password: 'qwerty',
      //   });
      //   const comments = [
      //     new Comment({
      //       author: user._id,
      //       content: 'test comment',
      //     }),
      //     new Comment({
      //       author: user._id,
      //       content: 'test comment',
      //     }),
      //     new Comment({
      //       author: user._id,
      //       content: 'test comment',
      //     }),
      //     new Comment({
      //       author: user._id,
      //       content: 'test comment',
      //     }),
      //     new Comment({
      //       author: user._id,
      //       content: 'test comment',
      //     }),
      //   ];
      //   User.addUser(user, (err, newUser)=>{
      //     const commentWithReply = new Comment({
      //       author: user._id,
      //       content: 'COMMENT WITH REPLY',
      //     });
      //     Comment.addComment(commentWithReply, () => {
      //       const reply = new Reply({
      //         author: user._id,
      //         content: 'REPLY TEST',
      //         replies_to: commentWithReply._id,
      //       });
      //       Reply.addReply(reply, ()=>{});
      //       // I don't know why but if modify or remove the next line reply doesn't register
      //       Comment.getCommentById(commentWithReply._id.toString(), (err, c)=>{});
      //     });
      //     // Store 5 comments
      //     comments.forEach((comment)=>{
      //       Comment.addComment(comment, ()=>{});
      //     });
      //     chai.request(app)
      //         .get(`/comments/${commentWithReply._id}?replies=false`)
      //         .end((err, res)=>{
      //           chai.expect(err).to.be.null;
      //           res.should.have.status(200);
      //           res.body.should.have.property('success').equal(true);
      //           res.body.should.have.property('message').equal('Comment fetched.');
      //           res.body.should.have.property('comment').to.be.a('object');
      //           res.body.comment.should.have.property('author').to.be.a('object');
      //           res.body.comment.author.should.have.property('_id').equal(newUser._id.toString());
      //           res.body.comment.author.should.not.have.property('password');
      //           res.body.comment.should.have.property('likes').to.be.a('array').that.is.empty;
      //           res.body.comment.should.have.property('replies').to.be.a('array').that.is.not.empty;
      //           res.body.comment.replies[0].should.be.a('string');
      //           res.body.comment.replies[0].should.not.have.property('author');
      //           res.body.comment.replies[0].should.not.have.property('_id');
      //           res.body.comment.replies[0].should.not.have.property('content');
      //           res.body.comment.should.have.property('content').to.be.a('string').equal(commentWithReply.content);
      //           res.body.comment.should.have.property('createdAt').to.be.a('string');
      //           res.body.comment.should.have.property('updatedAt').to.be.a('string');
      //           done();
      //         });
      //   });
      // });

      // End bugs
      it('Should return bad request 400 missing replies parameter', (done) => {
        const comment = new Comment({
          author: 'Invalid author id',
          content: 'Just to grab a comment ID',
          movie_id: '550',
        });
        chai.request(app)
            .get(`/comments/${comment._id}`)
            .end((err, res)=>{
              res.should.have.status(400);
              done();
            });
      });
      // Should return bad request 400 missing id parameter don't apply because router uses get /comments/ endpoint
      it('Should return NotFound 404 due invalid id and replies=false', (done) => {
        const comment = new Comment({
          author: 'Invalid author id',
          content: 'Just to grab a comment ID',
          movie_id: '550',
        });
        chai.request(app)
            .get(`/comments/${comment._id}?replies=false`)
            .end((err, res)=>{
              chai.expect(err).to.be.null;
              res.should.have.status(404);
              done();
            });
      });
      it('Should return NotFound 404 due invalid id and replies=true', (done) => {
        const comment = new Comment({
          author: 'Invalid author id',
          content: 'Just to grab a comment ID',
          movie_id: '550',
        });
        chai.request(app)
            .get(`/comments/${comment._id}?replies=true`)
            .end((err, res)=>{
              chai.expect(err).to.be.null;
              res.should.have.status(404);
              done();
            });
      });
      it('should return NotFound 404, db with 5 and replies=true comments and comment id not found', (done) =>{
        const user = new User({
          name: 'Comment Tester',
          email: 'comment@test.com',
          username: 'comments_tester',
          password: 'qwerty',
        });
        const comments = [
          new Comment({
            author: user._id,
            content: 'test comment 1',
          }),
          new Comment({
            author: user._id,
            content: 'test comment 2',
          }),
          new Comment({
            author: user._id,
            content: 'test comment 3',
          }),
          new Comment({
            author: user._id,
            content: 'test comment 4',
          }),
          new Comment({
            author: user._id,
            content: 'test comment 5',
          }),
        ];
        User.addUser(user, (err, newUser)=>{
          comments.forEach((comment)=>{
            Comment.addComment(comment, ()=>{});
          });
          const fakeComment = new Comment({
            author: user._id,
            content: 'FAKE comment',
          });
          chai.request(app)
              .get(`/comments/${fakeComment._id}?replies=true`)
              .end((err, res)=>{
                chai.expect(err).to.be.null;
                res.should.have.status(404);
                done();
              });
        });
      });
      it('should return NotFound 404, db with 5 and replies=false comments and comment id not found', (done) =>{
        const user = new User({
          name: 'Comment Tester',
          email: 'comment@test.com',
          username: 'comments_tester',
          password: 'qwerty',
        });
        const comments = [
          new Comment({
            author: user._id,
            content: 'test comment 1',
          }),
          new Comment({
            author: user._id,
            content: 'test comment 2',
          }),
          new Comment({
            author: user._id,
            content: 'test comment 3',
          }),
          new Comment({
            author: user._id,
            content: 'test comment 4',
          }),
          new Comment({
            author: user._id,
            content: 'test comment 5',
          }),
        ];
        User.addUser(user, (err, newUser)=>{
          comments.forEach((comment)=>{
            Comment.addComment(comment, ()=>{});
          });
          const fakeComment = new Comment({
            author: user._id,
            content: 'FAKE comment',
          });
          chai.request(app)
              .get(`/comments/${fakeComment._id}?replies=false`)
              .end((err, res)=>{
                chai.expect(err).to.be.null;
                res.should.have.status(404);
                done();
              });
        });
      });
    });

    describe('PUT /comments/edit', ()=>{
      it('should return Unauthorized 401 because missing Authorization header', (done) => {
        chai.request(app)
            .put('/comments/edit')
            .end((err, res)=>{
              chai.expect(err).to.be.null;
              res.should.have.status(401);
              done();
            });
      });
      it(`should return Unauthorized 401 because request has content and authenticated user don't match with comment author`, (done) => {
        const user = new User({
          name: 'Comment Tester',
          email: 'comment@test.com',
          username: 'comments_tester',
          password: 'qwerty',
        });
        // Add user to db and check
        User.addUser(user, ( ) => {
          User.getUserById(user._id, (err, fetchedUser) => {
            const req = {
              email: fetchedUser.email,
              password: 'qwerty',
            };
            chai.request(app)
                .post('/users/authenticate')
                .send(req)
                .then((res) => {
                  const token = res.body.token;
                  const req = {
                    _id: '5bca4972c5e7b6ab2470d427',
                    content: 'Comentario modificado, updateOne.',
                    author: '5bc4289508caa091dca1c0de',
                  };
                  chai.request(app)
                      .put('/comments/edit')
                      .set('Authorization', token)
                      .send(req)
                      .end((err, res)=>{
                        chai.expect(err).to.be.null;
                        res.should.have.status(401);
                        done();
                      });
                });
          });
        });
      });
      it(`should return Bad request 400 because request has likes and content parameter`, (done) => {
        const user = new User({
          name: 'Comment Tester',
          email: 'comment@test.com',
          username: 'comments_tester',
          password: 'qwerty',
        });
        // Add user to db and check
        User.addUser(user, ( ) => {
          User.getUserById(user._id, (err, fetchedUser) => {
            const req = {
              email: fetchedUser.email,
              password: 'qwerty',
            };
            chai.request(app)
                .post('/users/authenticate')
                .send(req)
                .then((res) => {
                  const token = res.body.token;
                  const req = {
                    _id: '5bca4972c5e7b6ab2470d427',
                    author: res.body.user._id,
                    content: 'Some edited content',
                    likes: ['asdasdasdasdasdasd'],
                  };
                  chai.request(app)
                      .put('/comments/edit')
                      .set('Authorization', token)
                      .send(req)
                      .end((err, res)=>{
                        chai.expect(err).to.be.null;
                        res.should.have.status(400);
                        done();
                      });
                });
          });
        });
      });
      it(`should return Bad request 400 because comment id is missing`, (done) => {
        const user = new User({
          name: 'Comment Tester',
          email: 'comment@test.com',
          username: 'comments_tester',
          password: 'qwerty',
        });
        // Add user to db and check
        User.addUser(user, ( ) => {
          User.getUserById(user._id, (err, fetchedUser) => {
            const req = {
              email: fetchedUser.email,
              password: 'qwerty',
            };
            chai.request(app)
                .post('/users/authenticate')
                .send(req)
                .then((res) => {
                  const token = res.body.token;
                  const req = {
                    author: res.body.user._id,
                  };
                  chai.request(app)
                      .put('/comments/edit')
                      .set('Authorization', token)
                      .send(req)
                      .end((err, res)=>{
                        chai.expect(err).to.be.null;
                        res.should.have.status(400);
                        done();
                      });
                });
          });
        });
      });
      it(`should return Bad request 400 because request doesn't have either content or likes`, (done) => {
        const user = new User({
          name: 'Comment Tester',
          email: 'comment@test.com',
          username: 'comments_tester',
          password: 'qwerty',
        });
        // Add user to db and check
        User.addUser(user, ( ) => {
          User.getUserById(user._id, (err, fetchedUser) => {
            const req = {
              email: fetchedUser.email,
              password: 'qwerty',
            };
            chai.request(app)
                .post('/users/authenticate')
                .send(req)
                .then((res) => {
                  const token = res.body.token;
                  const req = {
                    _id: '5bca4972c5e7b6ab2470d427',
                    author: res.body.user._id,
                  };
                  chai.request(app)
                      .put('/comments/edit')
                      .set('Authorization', token)
                      .send(req)
                      .end((err, res)=>{
                        chai.expect(err).to.be.null;
                        res.should.have.status(400);
                        done();
                      });
                });
          });
        });
      });
      it(`should return 200 because authenticated user match comment author and comment has been updated`, (done) => {
        const user = new User({
          name: 'Comment Tester',
          email: 'comment@test.com',
          username: 'comments_tester',
          password: 'qwerty',
        });
        // Add user to db and check
        User.addUser(user, ( ) => {
          User.getUserById(user._id, (err, fetchedUser) => {
            const req = {
              email: fetchedUser.email,
              password: 'qwerty',
            };
            chai.request(app)
                .post('/users/authenticate')
                .send(req)
                .then((res) => {
                  const token = res.body.token;
                  const comment = new Comment({
                    author: user._id,
                    content: 'test comment',
                    movie_id: '550',
                  });
                  const req = {
                    _id: comment._id.toString(),
                    content: 'Comentario modificado, updateOne.',
                    author: res.body.user._id,
                  };
                  Comment.addComment(comment, (err, comment)=>{
                    chai.request(app)
                        .put('/comments/edit')
                        .set('Authorization', token)
                        .send(req)
                        .end((err, res)=>{
                          chai.expect(err).to.be.null;
                          res.should.have.status(200);
                          res.body.should.have.property('success').equal(true);
                          res.body.should.have.property('message').equal('Comment edited.');
                          done();
                        });
                  });
                });
          });
        });
      });
      it(`should return 404 because authenticated user match comment author but comment id hasn't been found`, (done) => {
        const user = new User({
          name: 'Comment Tester',
          email: 'comment@test.com',
          username: 'comments_tester',
          password: 'qwerty',
        });
        // Add user to db and check
        User.addUser(user, ( ) => {
          User.getUserById(user._id, (err, fetchedUser) => {
            const req = {
              email: fetchedUser.email,
              password: 'qwerty',
            };
            chai.request(app)
                .post('/users/authenticate')
                .send(req)
                .then((res) => {
                  const token = res.body.token;
                  const comment = new Comment({
                    author: user._id,
                    content: 'test comment',
                    movie_id: '550',
                  });
                  const fakeComment = new Comment({
                    author: res.body.user._id,
                    content: 'Fake Tester',
                  });
                  const req = {
                    _id: fakeComment._id,
                    content: 'Comentario modificado, updateOne.',
                    author: res.body.user._id,
                  };
                  Comment.addComment(comment, (err, comment)=>{
                    chai.request(app)
                        .put('/comments/edit')
                        .set('Authorization', token)
                        .send(req)
                        .end((err, res)=>{
                          chai.expect(err).to.be.null;
                          res.should.have.status(404);
                          done();
                        });
                  });
                });
          });
        });
      });
      it(`should return success= false, message and error because an invalid id was sent`, (done) => {
        const user = new User({
          name: 'Comment Tester',
          email: 'comment@test.com',
          username: 'comments_tester',
          password: 'qwerty',
        });
        // Add user to db and check
        User.addUser(user, ( ) => {
          User.getUserById(user._id, (err, fetchedUser) => {
            const req = {
              email: fetchedUser.email,
              password: 'qwerty',
            };
            chai.request(app)
                .post('/users/authenticate')
                .send(req)
                .then((res) => {
                  const token = res.body.token;
                  const comment = new Comment({
                    author: user._id,
                    content: 'test comment',
                    movie_id: '550',
                  });
                  const req = {
                    _id: 'akldsaklsdasdkal;sdk;lakds',
                    content: 'Comentario modificado, updateOne.',
                    author: res.body.user._id,
                  };
                  Comment.addComment(comment, (err, comment)=>{
                    chai.request(app)
                        .put('/comments/edit')
                        .set('Authorization', token)
                        .send(req)
                        .end((err, res)=>{
                          chai.expect(err).to.be.null;
                          res.should.have.status(400);
                          res.body.should.have.property('success').equal(false);
                          res.body.should.have.property('message').equal('Fail trying to edit comment.');
                          res.body.should.have.property('error').to.be.a('object');
                          res.body.error.should.have.property('name').equal('CastError');
                          res.body.error.should.have.property('path').equal('_id');
                          done();
                        });
                  });
                });
          });
        });
      });
      it(`should return 200 and comment likes should be updated`, (done) => {
        const user = new User({
          name: 'Comment Tester',
          email: 'comment@test.com',
          username: 'comments_tester',
          password: 'qwerty',
        });
        // Add user to db and check
        User.addUser(user, ( ) => {
          User.getUserById(user._id, (err, fetchedUser) => {
            const req = {
              email: fetchedUser.email,
              password: 'qwerty',
            };
            chai.request(app)
                .post('/users/authenticate')
                .send(req)
                .then((res) => {
                  const token = res.body.token;
                  const comment = new Comment({
                    author: user._id,
                    content: 'test comment',
                    movie_id: '550',
                  });
                  const fakeUser = new User({});
                  const req = {
                    _id: comment._id.toString(),
                    likes: [res.body.user._id, fakeUser._id],
                  };
                  Comment.addComment(comment, (err, comment)=>{
                    chai.request(app)
                        .put('/comments/edit')
                        .set('Authorization', token)
                        .send(req)
                        .end((err, res)=>{
                          chai.expect(err).to.be.null;
                          res.should.have.status(200);
                          res.body.should.have.property('success').equal(true);
                          res.body.should.have.property('message').equal('Comment edited.');
                          done();
                        });
                  });
                });
          });
        });
      });
      it(`should return 404 because comment id hasn't been found`, (done) => {
        const user = new User({
          name: 'Comment Tester',
          email: 'comment@test.com',
          username: 'comments_tester',
          password: 'qwerty',
        });
        // Add user to db and check
        User.addUser(user, ( ) => {
          User.getUserById(user._id, (err, fetchedUser) => {
            const req = {
              email: fetchedUser.email,
              password: 'qwerty',
            };
            chai.request(app)
                .post('/users/authenticate')
                .send(req)
                .then((res) => {
                  const token = res.body.token;
                  const comment = new Comment({
                    author: user._id,
                    content: 'test comment',
                    movie_id: '550',
                  });
                  const fakeUser = new User({});
                  const fakeComment = new Comment({});
                  const req = {
                    _id: fakeComment._id.toString(),
                    likes: [res.body.user._id, fakeUser._id],
                  };
                  Comment.addComment(comment, (err, comment)=>{
                    chai.request(app)
                        .put('/comments/edit')
                        .set('Authorization', token)
                        .send(req)
                        .end((err, res)=>{
                          chai.expect(err).to.be.null;
                          res.should.have.status(404);
                          done();
                        });
                  });
                });
          });
        });
      });
      it(`should return 200 and success=false,message and error because comment likes have one invalid id`, (done) => {
        const user = new User({
          name: 'Comment Tester',
          email: 'comment@test.com',
          username: 'comments_tester',
          password: 'qwerty',
        });
        // Add user to db and check
        User.addUser(user, ( ) => {
          User.getUserById(user._id, (err, fetchedUser) => {
            const req = {
              email: fetchedUser.email,
              password: 'qwerty',
            };
            chai.request(app)
                .post('/users/authenticate')
                .send(req)
                .then((res) => {
                  const token = res.body.token;
                  const comment = new Comment({
                    author: user._id,
                    content: 'test comment',
                    movie_id: '550',
                  });
                  const req = {
                    _id: comment._id.toString(),
                    likes: [res.body.user._id, 'Invalid ID 23$@#'],
                  };
                  Comment.addComment(comment, (err, comment)=>{
                    chai.request(app)
                        .put('/comments/edit')
                        .set('Authorization', token)
                        .send(req)
                        .end((err, res)=>{
                          chai.expect(err).to.be.null;
                          res.should.have.status(400);
                          res.body.should.have.property('success').equal(false);
                          res.body.should.have.property('message').equal('Fail trying to edit comment.');
                          res.body.should.have.property('error').to.be.a('object');
                          res.body.error.should.have.property('name').equal('CastError');
                          res.body.error.should.have.property('path').equal('likes');
                          done();
                        });
                  });
                });
          });
        });
      });
    });

    describe('DELETE /comments/delete/:id', () => {
      it('should return Unauthorized 401 due missing Authorization header', (done) => {
        chai.request(app)
            .delete(`/comments/delete/5bcf8d520952c65fbcee6cb9`)
            .end((err, res)=>{
              chai.expect(err).to.be.null;
              res.should.have.status(401);
              done();
            });
      });

      it(`should return Unauthorized 401 because authenticated user don't match with comment author`, (done) => {
        const user = new User({
          name: 'Comment Tester',
          email: 'comment@test.com',
          username: 'comments_tester',
          password: 'qwerty',
        });
        // Add user to db and check
        User.addUser(user, ( ) => {
          User.getUserById(user._id, (err, fetchedUser) => {
            const comment = new Comment({
              author: user._id,
              content: 'DELETINGGGGGGGGGGGGGGGG',
              movie_id: '550',
            });

            Comment.addComment(comment, (err, comment)=>{
              const fakeUser = new User({
                name: 'Fake Tester',
                email: 'faket@test.com',
                username: 'fake_tester',
                password: 'qwerty',
              });
              const req = {
                email: fakeUser.email,
                password: 'qwerty',
              };
              User.addUser(fakeUser, (err, fake)=>{
                chai.request(app)
                    .post('/users/authenticate')
                    .send(req)
                    .then((res) => {
                      const token = res.body.token;
                      chai.request(app)
                          .delete(`/comments/delete/${comment._id}`)
                          .set('Authorization', token)
                          .send(req)
                          .end((err, res)=>{
                            chai.expect(err).to.be.null;
                            res.should.have.status(401);
                            done();
                          });
                    });
              });
            });
          });
        });
      });

      it(`should return 404 because comment is not found`, (done) => {
        const user = new User({
          name: 'Comment Tester',
          email: 'comment@test.com',
          username: 'comments_tester',
          password: 'qwerty',
        });
        // Add user to db and check
        User.addUser(user, ( ) => {
          User.getUserById(user._id, (err, fetchedUser) => {
            const req = {
              email: fetchedUser.email,
              password: 'qwerty',
            };
            chai.request(app)
                .post('/users/authenticate')
                .send(req)
                .then((res) => {
                  const token = res.body.token;
                  chai.request(app)
                      .delete('/comments/delete/5bcf8d520952c65fbcee6cb9')
                      .set('Authorization', token)
                      .send(req)
                      .end((err, res)=>{
                        chai.expect(err).to.be.null;
                        res.should.have.status(404);
                        done();
                      });
                });
          });
        });
      });

      it(`should return 200, authenticated user match with comment author and comment with no replies was deleted`, (done) => {
        const user = new User({
          name: 'Comment Tester',
          email: 'comment@test.com',
          username: 'comments_tester',
          password: 'qwerty',
        });
        // Add user to db and check
        User.addUser(user, ( ) => {
          User.getUserById(user._id, (err, fetchedUser) => {
            const req = {
              email: user.email,
              password: 'qwerty',
            };
            const comment = new Comment({
              author: user._id,
              content: 'DELETINGGGGGGGGGGGGGGGG',
              movie_id: '550',
            });
            Comment.addComment(comment, (err, comment)=>{
              chai.request(app)
                  .post('/users/authenticate')
                  .send(req)
                  .then((res) => {
                    const token = res.body.token;
                    chai.request(app)
                        .delete(`/comments/delete/${comment._id}`)
                        .set('Authorization', token)
                        .end((err, res)=>{
                          chai.expect(err).to.be.null;
                          res.should.have.status(200);
                          res.body.should.have.property('success').equal(true);
                          res.body.should.have.property('message').equal('Comment deleted.');
                          done();
                        });
                  });
            });
          });
        });
      });

      it(`should return 404, authenticated user match with comment author and comment with invalid replies`, (done) => {
        const user = new User({
          name: 'Comment Tester',
          email: 'comment@test.com',
          username: 'comments_tester',
          password: 'qwerty',
        });
        // Add user to db and check
        User.addUser(user, ( ) => {
          User.getUserById(user._id, (err, fetchedUser) => {
            const req = {
              email: user.email,
              password: 'qwerty',
            };
            const comment = new Comment({
              author: user._id,
              content: 'DELETINGGGGGGGGGGGGGGGG',
              movie_id: '550',
            });
            const reply1 = new Reply({
              author: '5bcf8d52095#2c65fbcee6cb9',
              content: 'Invalid User id on author',
              replies_to: comment._id,
            });
            const reply2 = new Reply({
              author: comment.author,
              content: 'User id on author',
              replies_to: comment._id,
            });
            const reply3 = new Reply({
              author: comment.author,
              content: 'User id on author',
              replies_to: comment._id,
            });
            Comment.addComment(comment, (err, comment)=>{
              Reply.addReply(reply1, ()=>{});
              Reply.addReply(reply2, ()=>{});
              Reply.addReply(reply3, (err, reply)=>{});

              chai.request(app)
                  .post('/users/authenticate')
                  .send(req)
                  .then((res) => {
                    const token = res.body.token;
                    chai.request(app)
                        .delete(`/comments/delete/${comment._id}`)
                        .set('Authorization', token)
                        .end((err, res)=>{
                          chai.expect(err).to.be.null;
                          res.should.have.status(404);
                          res.body.should.have.property('success').equal(false);
                          res.body.should.have.property('message').equal('Error while trying to delete comment,  comments reply  not found.');
                          done();
                        });
                  });
            });
          });
        });
      });

      it(`should return 200, authenticated user match with comment author and comment with replies`, (done) => {
        const user = new User({
          name: 'Comment Tester',
          email: 'comment@test.com',
          username: 'comments_tester',
          password: 'qwerty',
        });
        // Add user to db and check
        User.addUser(user, ( ) => {
          User.getUserById(user._id, (err, fetchedUser) => {
            const req = {
              email: user.email,
              password: 'qwerty',
            };
            const comment = new Comment({
              author: user._id,
              content: 'DELETINGGGGGGGGGGGGGGGG',
              movie_id: '550',
            });
            const reply1 = new Reply({
              author: '5bcf8d520952c65fbcee6cb9',
              content: 'Invalid User id on author',
              replies_to: comment._id,
            });
            const reply2 = new Reply({
              author: comment.author,
              content: 'User id on author',
              replies_to: comment._id,
            });
            const reply3 = new Reply({
              author: comment.author,
              content: 'User id on author',
              replies_to: comment._id,
            });
            Comment.addComment(comment, (err, comment)=>{
              Reply.addReply(reply1, ()=>{});
              Reply.addReply(reply2, ()=>{});
              Reply.addReply(reply3, (err, reply)=>{});

              chai.request(app)
                  .post('/users/authenticate')
                  .send(req)
                  .then((res) => {
                    const token = res.body.token;
                    chai.request(app)
                        .delete(`/comments/delete/${comment._id}`)
                        .set('Authorization', token)
                        .end((err, res)=>{
                          chai.expect(err).to.be.null;
                          res.should.have.status(200);
                          res.body.should.have.property('success').equal(true);
                          res.body.should.have.property('message').equal('Comment deleted.');
                          done();
                        });
                  });
            });
          });
        });
      });
    });
  });
});
