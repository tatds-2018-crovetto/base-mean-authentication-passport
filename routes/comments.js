const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const passport = require('passport');
const Comment = require('../models/comment');
const Reply = require('../models/reply');

router.get('/', (req, res, next) =>{
  Comment.getAll((comments, err)=>{
    if (comments) {
      res.json({
        success: true,
        message: 'Comments fetched.',
        comments: comments});
    } else {
      res.status(400).json({
        success: false,
        message: 'Error trying to fetch comments.',
        error: err});
    }
  });
});
router.post('/add',
    passport.authenticate('jwt', {session: false}),
    (req, res, next)=>{
      // Checking that the user authenticated
      // is the same one of the request author
      if (req.user._id.toString() === req.body.author) {
        // Checking body parameters are defined
        if (req.body.author && req.body.content && req.body.movie_id) {
          const comment = new Comment({
            author: req.body.author,
            content: req.body.content,
            movie_id: req.body.movie_id,
          });
          Comment.addComment(comment, (err, comment) =>{
            if (err) {
              res
                  .status(400)
                  .json({
                    success: false,
                    message: 'Fail to store comment',
                    error: err});
            } else {
              res.json({
                success: true,
                message: 'Comment stored.',
                comment: comment});
            }
          });
        } else {
          res.sendStatus(400);
        }
      } else {
        res.sendStatus(401);
      }
    });
router.get('/:id', (req, res, next)=>{
  const id = req.params.id;
  const replies = req.query.replies;
  if (id && replies) {
    if (replies === 'true') {
      Comment.getCommentWithReplies(id, (comment, err)=>{
        if (comment === null) {
          res.sendStatus(404);
          // res.json({
          //   success: false,
          //   message: `Fail trying to fetch comment and replies.`,
          //   error: err});
        } else {
          res.json({
            success: true,
            message: `Comment w/replies fetched.`,
            comment: comment});
        }
      });
    } else {
      Comment.getCommentById(id, (comment, err)=>{
        if (comment === null) {
          res.sendStatus(404);
        } else {
          res.json({
            success: true,
            message: `Comment fetched.`,
            comment: comment});
        }
      });
    }
  } else {
    res.sendStatus(400);
  }
});
router.put(
    '/edit',
    passport.authenticate('jwt', {session: false}),
    (req, res)=>{
      if (req.body._id) {
        if (req.body.content && req.body.likes) {
          res.sendStatus(400);
        } else {
          if (req.body.content) {
            if (req.user._id.toString() === req.body.author) {
              Comment.editComment(req.body, (err, response)=> {
                if (err) {
                  res.status(400).json({
                    success: false,
                    message: `Fail trying to edit comment.`,
                    error: err,
                  });
                } else {
                  if (response.nModified === 0) {
                    res.sendStatus(404);
                  } else {
                    res.json({success: true, message: `Comment edited.`});
                  }
                }
              });
            } else res.sendStatus(401);
          } else {
            if (req.body.likes) {
              Comment.editComment(req.body, (err, response)=> {
                if (err) {
                  res.status(400).json({
                    success: false,
                    message: `Fail trying to edit comment.`,
                    error: err,
                  });
                } else {
                  if (response.nModified === 0) {
                    res.sendStatus(404);
                  } else {
                    res.json({success: true, message: `Comment edited.`});
                  }
                }
              });
            } else res.sendStatus(400);
          }
        }
      } else res.sendStatus(400);
    });
router.delete(
    '/delete/:id',
    passport.authenticate('jwt', {session: false}),
    (req, res, next)=>{
      const id = req.params.id;
      // Checking query parameters are defined
      if (id) {
        Comment.getCommentById(id, (comment) => {
          if (comment === null) {
            res.sendStatus(404);
          } else {
            if (req.user._id.toString() !== comment.author._id.toString()) {
              res.sendStatus(401);
            } else {
              if (comment.replies.length > 0) {
                const i = comment.replies.length;
                Reply.deleteCommentReplies(comment._id, (err, response)=>{
                  if (response.n.toString() !== i.toString()) {
                    res.status(404).json({
                      success: false,
                      message: 'Error while trying to delete comment, ' +
                                ' comments reply  not found.',
                    });
                  } else {
                    res.status(200)
                        .json({success: true, message: `Comment deleted.`});
                  }
                });
              } else {
                Comment.deleteComment(id, (err, response) => {
                  if (response.n === 0) {
                    res.sendStatus(404);
                  } else {
                    res.json({success: true, message: `Comment deleted.`});
                  }
                });
              }
            }
          }
        });
      } else res.sendStatus(400);
    });
router.get('/movies/:movieId', (req, res, next)=>{
  const movieId = req.params.movieId;
  const replies = req.query.replies;
  if (movieId && replies) {
    if (replies === 'true') {
      Comment.getCommentByMovieWithReplies(movieId, (comments, err)=>{
        if (comments === null) {
          res.status(404).json({
            success: false,
            message: `Fail trying to fetch comment
             and the corresponding replies.`,
            error: err});
        } else {
          res.json({
            success: true,
            message: `Comment w/replies fetched.`,
            comments: comments});
        }
      });
    } else {
      Comment.getCommentByMovieId(movieId, (comments, err)=>{
        if (comments === null) {
          res.status(404).json({
            success: false,
            message: `Fail trying to fetch comment.`,
            error: err});
        } else {
          res.json({
            success: true,
            message: `Comment fetched.`,
            comments: comments});
        }
      });
    }
  } else {
    res.sendStatus(400);
  }
});
module.exports = router;
