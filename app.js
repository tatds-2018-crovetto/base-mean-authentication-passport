/* eslint-disable no-console */
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const cors = require('cors');
const logger = require('morgan');
const config = require('./config/config');
const sassMiddleware = require('node-sass-middleware');
const usersRouter = require('./routes/users');
const commentsRouter = require('./routes/comments');
const repliesRouter = require('./routes/replies');
const indexRouter = require('./routes/index');
const passport = require('passport');

console.log(`Environment database ${config.database}`);
// mongoose set up
const mongoose = require('mongoose');
mongoose.connect(config.database);
// CONNECTION HANDLERS
mongoose.connection.on('connected', () => {
  console.log('Connected to database ' + config.database);
});
// Retry connection
const connectWithRetry = () => {
  console.log('MongoDB connection with retry');
  return mongoose.connect(config.database);
};
// Exit application on error
mongoose.connection.on('error', (err) => {
  console.log(`MongoDB connection error: ${err}`);
  setTimeout(connectWithRetry, 5000);
  // process.exit(-1)
});
const app = express();

const port = config.port;

app.use(cors());
app.use(bodyParser.json());
// Passport set up
require('./config/passport')(passport);
app.use(passport.initialize());
app.use(passport.session());

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cookieParser());
app.use(sassMiddleware({
  src: path.join(__dirname, 'public'),
  dest: path.join(__dirname, 'public'),
  indentedSyntax: false, // true = .sass and false = .scss
  sourceMap: true,
}));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/replies', repliesRouter);
app.use('/comments', commentsRouter);

app.listen(port, () => {
  console.log('its alive alive');
});

module.exports = app;
